package rail_reboosted.rail_reboosted;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.player.UseItemCallback;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MovementType;
import net.minecraft.item.FireworkItem;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import rail_reboosted.rail_reboosted.config.ModConfig;

public class RailReboosted implements ModInitializer {

    @Override
    public void onInitialize() {
        ModConfig.initialise();

        float RAILBOOST_HORIZONTAL_VELOCITY = ModConfig.INSTANCE.fireworkStrength;

        UseItemCallback.EVENT.register((player, world, hand) -> {
            if (!(world instanceof ServerWorld)) {
                return new TypedActionResult<>(ActionResult.PASS, player.getStackInHand(hand));
            }

            ItemStack stack = player.getStackInHand(hand);
            if (!(stack.getItem() instanceof FireworkItem)) {
                return new TypedActionResult<>(ActionResult.PASS, player.getStackInHand(hand));
            }

            if (player.hasVehicle() == false || (player.getVehicle().getType() != EntityType.MINECART) || !ModConfig.INSTANCE.minecartsOnly) {
                return new TypedActionResult<>(ActionResult.PASS, player.getStackInHand(hand));
            }

            Entity vehicle = player.getVehicle();
            if (!player.abilities.creativeMode) {
                stack.decrement(1);
            }
            player.swingHand(hand);
            player.addExhaustion(0.05F);

            Direction facing = player.getHorizontalFacing();
            String cardinal = facing.toString();


            Vec3d boost = new Vec3d(0, 0, 0);
            if (cardinal.equals("north")) {
                boost = new Vec3d(0, 0, -RAILBOOST_HORIZONTAL_VELOCITY);
            } else if (cardinal.equals("east")) {
                boost = new Vec3d(RAILBOOST_HORIZONTAL_VELOCITY, 0, 0);
            } else if (cardinal.equals("west")) {
                boost = new Vec3d(-RAILBOOST_HORIZONTAL_VELOCITY, 0, 0);
            } else if (cardinal.equals("south")) {
                boost = new Vec3d(0, 0, RAILBOOST_HORIZONTAL_VELOCITY);
            }

            vehicle.addVelocity(boost.x, boost.y, boost.z);
            vehicle.velocityDirty = true;
            vehicle.move(MovementType.SELF, boost);

            BlockPos pos = player.getBlockPos();
            world.playSound(null, pos.getX(), pos.getY(), pos.getZ(), SoundEvents.ENTITY_FIREWORK_ROCKET_LAUNCH, SoundCategory.PLAYERS, 1.0F, 1.0F);

            return new TypedActionResult<>(ActionResult.SUCCESS, player.getStackInHand(hand));
        });
    }
}
