package rail_reboosted.rail_reboosted.config;

public class ConfigInstance {

	public int fireworkStrength;
	public boolean minecartsOnly;
	
	public ConfigInstance() {
		fireworkStrength = 6;
		minecartsOnly = true;
	}
	
}
